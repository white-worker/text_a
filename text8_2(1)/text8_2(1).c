#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//打印一个数的二进制位的奇数位和偶数位
int main()
{
	int n = 0;
	printf("请输入一个数字");
	scanf("%d",&n);
	//打印偶数位
	for (int i = 31; i >= 1; i -= 2)
	{
		printf("%d ", (n >> i) & 1);
	}
	printf("\n");
	//打印奇数位
	for (int i = 30; i >= 1; i -= 2)
	{
		printf("%d ", (n >> i) & 1);
	}
	return 0;
}