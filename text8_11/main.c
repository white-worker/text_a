#include <stdio.h>
void print1(int arr[3][3],int r,int c)
{
    int i = 0;
    int j = 0;
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
}
void print2(int (*pa)[3], int r, int c)
{
    int i = 0;
    int j = 0;
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            printf("%d ", *(*(pa + i) + j));
        }
        printf("\n");
    }
}
int main()
{
    //打印arr数组
    //print1是标准传参打印
    //print2是数组执政传参打印
    int arr[3][3]={ {0,1,2},{3,4,5},{6,7,8} };
    print1(arr,3,3);
    printf("\n");
    print2(arr,3,3);
    return 0;
}
