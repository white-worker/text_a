#include <stdio.h>
#include <string.h>
#include <assert.h>
#define _CRT_SECURE_NO_WARNINGS 

//写一个程序，左旋转n个字符
//例如：ABCDEF 左旋两个字符得到
//      CDEFAB
//方法一： 如： ABCDEF 
//             1.先将A取出    A   BCDEF
//             2.将CBCDEF向前    BCDEF
//             3.将A放到最后     BCDEFA
//             左旋几次就取出几个数
//void string_left_move(char* str,int k)
//{
//	int n = strlen(str);
//	int i = 0;
//	for (i = 0; i < k; i++)
//	{
//		//每次左旋一个字符
//		int j = 0;
//		//将第一个字符取出
//		char tmp = *str;
//		//将后面的字符向前
//		for (j = 0; j < n - 1; j++)
//		{
//			*(str + j) = *(str + (j + 1));
//		}
//		//将第一个字符放到最后
//		*(str + n - 1) = tmp;
//	}
//}
//int main()
//{
//	char arr[10] = "ABCDEF";
//	//printf("请输入想要旋转的数：》");
//	int k = 3;
//	//scanf("%d", &k);
//	string_left_move(arr,k);
//	printf("%s", arr);
//	return 0;
//}
//方法二： 如： ABCDEF
//             以左旋两个数为例
//             1.将ABCDEF 看成    AB CDEF
//             2.将AB逆序CDEF逆序 BA FEDC
//             3.整体逆序         CDEFAB
void reverse(char* left, char* right)
{
	assert(left);
	assert(right);
	while (left < right)
	{
		char tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}
void string_left_move(char* str, int k)
{
	int n = strlen(str);
	reverse(str,str + k - 1);
	reverse(str + k, str + n - 1);
	reverse(str,str + n - 1);
}
int main()
{
	char arr[10] = "ABCDEF";
	int k = 2;
	string_left_move(arr, k);
	printf("%s", arr);
	return 0;
}