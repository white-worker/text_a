
#include <stdio.h>
#include <assert.h>
//模拟实现strstr的实现
char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);
	char* s1 = NULL;
	char* s2 = NULL;
	char* cp = str1;
	while (*cp)
	{
		s1 = cp;
		s2 = str2;
		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
		{
			return cp;
		}
		cp++;
	}
	return NULL;
}
int main()
{
	char arr1[] = "abcdef";
	char arr2[] = "abf";
	char* ret = my_strstr(arr1,arr2);
	if (ret == NULL)
	{
		printf("没有找到\n");
	}
	else
	{
		printf("%s", ret);
	}
	return 0;
}