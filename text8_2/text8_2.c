#include <stdio.h>
#include <assert.h>
//计算字符串的长度
//size_t是unsigned int
size_t my_stylen(const char*arr)
{
	assert(arr != NULL);//断言
	size_t count = 0;
	while (*arr != '\0')
	{
		count++;
		arr++;
	}
	return count;
}
//初级版，仅仅可以计算正数
int NumberOf(int n)
{
	int count = 0;
	while (n)
	{
		if (n % 2 == 1)
		{
			count++;
		}
		n /= 2;
	}
	return count;
}
//进阶版
int NumberOf1(int n)
{
	int count = 0;
	int i = 0;
	for (i = 0; i < 32; i++)
	{
		if (((n >> i) & 1) == 1)
		{
			count++;
		}
	}
	return count;
}
//最强版
int NumberOf2(int n)
{
	int count = 0;
	while (n)
	{
		n = n & (n - 1);
		count++;
	}
	return count;
}
int main()
{
	//计算一个数字的二进制数的1的个数
	int n = 15;
	int ret = NumberOf(n);
	int ret1 = NumberOf1(n);
	int ret2 = NumberOf2(n);
	printf("ret=  %d\n", ret);
	printf("ret1= %d\n", ret1);
	printf("ret2= %d\n", ret2);
	//输入两个数，计算两个数的二进制数不同位的个数
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	int count = 0;
	for (int i = 0; i < 32; i++)
	{
		if ((a >> i) & 1 != (b >> i) & 1)
		{
			count++;
		}
	}
	char arr[] = "abcde";
	printf("%d\n", my_stylen(arr));
	return 0;
}