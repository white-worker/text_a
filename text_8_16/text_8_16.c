#include <stdio.h>

void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
		arr++;
	}
	printf("\n");
}
void Swap(char* buf1, char* buf2,int width)
{
	int i = 0;
	for (i = 0; i < width - 1; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}
int arr_sort(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
void bubble_sort(void* base, int sz, int width, int (*cmp)(const void* e1, const void* e2))
{
	//趟数
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		//一趟的交换
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				//交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
			}
		}
	}
}
//通过冒泡排序的方法自己写一个函数来对任何类型的元素进行排序
int main()
{
	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	/*int width = sizeof(arr[0]);*/
	print(arr, sz);
	bubble_sort(arr,sz, sizeof(arr[0]) ,arr_sort);
	print(arr, sz);
	return 0;
}