#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//对于一个每一行递增的矩阵，请写一个程序
//来寻找一个数，且时间复杂度小于O(N)

int found_num(int arr[][3],int* px,int* py,int k)
{
	int x = 0;
	int y = *py - 1;
	while ( x < *px && y >= 0)
	{
		if ( arr[x][y] < k )
		{
			x++;
		}
		else if (arr[x][y] > k)
		{
			y--;
		}
		else
		{
			*px = x;
			*py = y;
			return 1;
		}
	}
	return 0;
}
int main()
{
	int arr[3][3] = {1,2,3,4,5,6,7,8,9};
	int k = 0;
	int x = 3;
	int y = 3;
	printf("请输入查询的数：》");
	scanf("%d", &k);
	int ret = found_num(arr,&x,&y,k);
	if (ret == 1)
	{
		printf("%d %d\n", x, y);
		printf("找到了\n");
	}
	if (ret == 0)
	{
		printf("没有找到\n");
	}
	return 0;
}