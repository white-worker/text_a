#include <stdio.h>

int main()
{
    //整形数组
    int a[] = { 1,2,3,4 };
    printf("%d\n", sizeof(a));//16
    printf("%d\n", sizeof(a + 0));//4/8 - a + 0 是第一个元素的地址，sizeof(a + 0)计算的是地址的大小
    printf("%d\n", sizeof(*a));//4 - *a是首元素，故计算的是第一个元素的大小
    printf("%d\n", sizeof(a + 1));//4/8 - a + 1 是第二个元素的地址，故计算的是地址的大小
    printf("%d\n", sizeof(a[1]));//4 - a[1]是第二个元素，故计算的是第二个元素的大小
    printf("%d\n", sizeof(&a));//4/8 - &a虽然是整个数组的地址，但是也是地址，sizeof（&a）计算的是地址的大小
    printf("%d\n", sizeof(*&a));//16 - & * 相抵消
    printf("%d\n", sizeof(&a + 1));//4/8 - &a + 1是数组后空间的地址，故计算的是地址的大小
    printf("%d\n", sizeof(&a[0]));//4/8 - &a[0]是第一个元素的地址，故计算的是地址的大小
    printf("%d\n", sizeof(&a[0] + 1));//4/8 - &a[0]+1 是第二个元素的地址，故计算的是地址的大小

    //字符数组
    char arr[] = { 'a','b','c','d','e','f' };
    printf("%d\n",sizeof(arr));//6
    printf("%d\n",sizeof(arr + 0));//4/8 - arr + 0 是数组第一个元素的地址
    printf("%d\n",sizeof(*arr));//1 - *arr是第一个元素
    printf("%d\n",sizeof(arr[1]));//1 — arr[1]是第二个元素
    printf("%d\n",sizeof(&arr));//4/8 - &arr是整个数组的地址
    printf("%d\n",sizeof(&arr + 1));//4/8 - &arr + 1是数组后空间的地址，故计算的是地址的大小
    printf("%d\n",sizeof(&arr[0] + 1));//4/8 - &arr[0] + 1是第二个元素的地址
    return 0;
}
