
#include <stdio.h>
#include <stddef.h>
#include <assert.h>
//模拟实现memcpy函数
//模拟实现memmove
void* my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}

void print_cpy(int* str,int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", str[i]);
	}
	printf("\n");
}

void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	if (dest < src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}

void print_move(int* str, int szz)
{
	for (int i = 0; i < szz; i++)
	{
		printf("%d ", str[i]);
	}
	printf("\n");
}

void print_set(int* str, int num)
{
	for (int i = 0; i < num; i++)
	{
		printf("%d ",str[i]);
	}
}
int main()
{
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[10] = { 0 };
	my_memcpy(arr2, arr1, 20);
	int sz = sizeof(arr2) / sizeof(arr2[0]);
	print_cpy(arr2,sz);
	int arr3[10] = { 1,2,3,4,5,6,7,8,9,10 };
	my_memmove(arr3, arr3 + 2, 20);
	int szz = sizeof(arr3) / sizeof(arr3[0]);
	print_move(arr3, szz);
	int arr4[10] = { 1,2,3,4,5,6,7,8,9,10 };
	memset(arr4, 0, 40);
	print_set(arr4, 10);
	return 0;
}