#include <stdio.h>
#include <assert.h>
//模拟实现一个strlen
//模拟实现一个strcat
int my_strlen(const char* str)
{
    int count = 0;      //计数器
    assert(str != NULL);
    while (*str != '\0')
    {
        count++;
        str++;
    }
    return count;
}
char* my_strcat(char* dest, const char* src)
{
    char* ret = dest;
    assert(dest && src);
    //在dest中找到'\0'
    while (*dest)
    {
        dest++;
    }
    //覆盖
    while (*dest++ = *src++)
    {
        ;
    }
    return ret;
}
int main()
{
    char arr[] = "abc";
    int len = my_strlen(arr);
    printf("%d\n",len);
    char arr1[20] = "hello ";
    char arr2[6] = "world";
    printf("%s\n",my_strcat(arr1,arr2));
    return 0;

}
