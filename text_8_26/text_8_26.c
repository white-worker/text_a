#include <stdio.h>
#include <assert.h>
int my_strcmp(const char* s1, const char* s2)
{
	//assert(s1 && s2);
	assert(s1 != NULL && s2 != NULL);
	while (*s1 == *s2)
	{
		if (*s1 == '\0')
		{
			return 0;
		}
		s1++;
		s2++;
	}
	return *s1 - *s2;
}
//模拟实现strcmp
int main()
{
	char* p = "abc";
	char* q = "abd";
	int ret = my_strcmp(p, q);
	if (ret == 0)
	{
		printf("相等\n");
	}
	else if (ret > 0)
	{
		printf("第一个大\n");
	}
	else
	{
		printf("第二个大\n");
	}
	return 0;
}