#define _CRT_SECURE_NO_WARNINGS 
//模拟实现strcpy
//strcpy--字符串拷贝
#include <stdio.h>
#include <string.h>
//int main()
//{
//
//	char arr1[20] ="xxxxxxxxxx";
//	char arr2[] = "hello";
//	strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}
struct Stu
{
	char name[20];
	char Uid[20];
};
int main()
{
	int i = 0;
	struct Stu z = { "张普元",123 };
	printf("请输入账户:>\n");
	scanf("%s", z.name);
	if (strcmp(z.name, "张普元") == 0)  //我想用一个自定义函数忧化一下if判断，就是令一个自定义函数来判断z.name和"张普元"相等，最后直接判断我的自定义函数的返回值来进行if判断
	{
		printf("账号存在，请输入密码：");
		goto this;
	}
	else
	{
		printf("账号不存在，正在跳转注册界面");
		goto that;
	}
	this:printf("请输入你的密码：");
	//多此一举的程序，想让用户只可以输入三次密码
	for (i = 0; i < 3; i++)
	{
		scanf("%s", z.Uid);
		if (strcmp(z.Uid, "123") == 0)
		{
			printf("登录成功，正在进入首页");
			break;
		}
		else
		{
			printf("密码输入错误，请重新登录！！！");
		}
	}
	if (i == 3)
	{
		printf("密码输入三次不正确，禁止登录");
	}
that:printf("请完成注册");
	return 0;
}
