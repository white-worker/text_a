#include <stdio.h>
int main()
{
    char* ps="hellowword";
    printf("%c\n",* ps);
    //关于多个指针指向同一个常量字符串是，不必开辟多个地址！！！
    char arr1[] = {"hello hh"};
    char arr2[] = {"hello hh"};
    const char* srt1 = "hello hh";
    const char* srt2 = "hello hh";
    if (arr1 == arr2)
    {
        printf("same\n");
    }
    else
    {
        printf("not same\n");
    }
    if (srt1 == srt2)
    {
        printf("same\n");
    }
    else
    {
        printf("not same");
    }
    //数组指针，是指向数组的地址
    int arr[10]= { 1,2,3,4,5 };
    int (*parr)[10] = &arr;
    printf("%d\n",*parr);
    return 0;
}
