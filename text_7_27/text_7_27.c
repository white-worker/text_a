#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
//指针的类型决定了
//1.解引用的权限
//2.步长
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	char* pc = arr;
//	printf("%p\n", p);
//	printf("%p\n", pc);
//
//	return 0;
//}
//指针要初始化，若不初始化，默认是随机值，就会导致是一个野指针。
//int main()
//{
//	//当不知道指针应该初始化什么地址时，直接初始化NULL
//	//int* p = NULL;
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i; i < 10; i++)
//	{
//		*(p + i) = 1;
//	}
//	return 0;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//指针减指针所得到的是两个指针之间的元素个数
//	printf("%d\n", &arr[9] - &arr[0]);
//	int* p = arr;
//	int* pend = arr + 9;
//	while (p<=pend)
//	{
//		printf("%d\n", *p++);
//	}
//	return 0;
//}
//计算"abc"的长度
//指针
int my_strlen(char* str)
{
	char* start = str;
	while (*str != '\0')
	{
		str++;
	}
	return str - start;
}
int main()
{
	int len = my_strlen("abc");
	printf("%d", len);
	return 0;
}