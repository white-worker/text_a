#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//结构体的学习
//结构体里可以有数组、字符、字符串、指针、其他结构体
struct B
{
	char a;
	int b;
	double c;
};
struct stu
{
	struct B ss;
	char name[20];
	int age;
	char id[20];
};
void print1(struct stu t)
{
	printf("%c %d %lf %s %d %s\n", t.ss.a, t.ss.b, t.ss.c, t.name, t.age, t.id);
}
void print2(struct stu *ps)
{
	printf("%c %d %lf %s %d %s\n", ps->ss.a, ps->ss.b, ps->ss.c, ps->name, ps->age, ps->id);
}
int main()
{
	struct stu s = { {'w',3,3.14},"弓长",99,"w20234058945"};//初始化对象
	//结构体的实现用：  .   ->
	printf("%c\n", s.ss.a);
	printf("%s\n", s.id);
	//对于 -> 是在指针里使用
	struct stu* ps = &s;
	printf("%c\n", (*ps).ss.a);
	printf("%c\n", ps->ss.a);
	//结构体的传参
	print1(s);//传值调用
	print2(&s);//传址调用
	return 0;
}